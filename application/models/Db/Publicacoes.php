<?php

class Application_Model_Db_Publicacoes extends ZendPlugin_Db_Table 
{
    protected $_name = "publicacoes";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos do publicacao
     *
     * @param int $id - id do publicacao
     *
     * @return array - rowset com fotos do publicacao
     */
    public function getFotos($id)
    {
        if(!$publicacao = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($publicacao_fotos = $publicacao->findDependentRowset('Application_Model_Db_PublicacoesFotos')){
            foreach($publicacao_fotos as $publicacao_foto){
                $fotos[] = Is_Array::utf8DbRow($publicacao_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    public function getLast()
    {
        $publicacao = $this->getLasts(1);
        return $publicacao[0];
    }

    public function getLasts($limit=3,$order='data_edit desc')
    {
        $_publicacoes = $this->fetchAll('status_id=1',$order,$limit);
        
        if(count($_publicacoes)){
            $publicacoes = Is_Array::utf8DbResult($_publicacoes);
            
            foreach($publicacoes as &$publicacao){
                $publicacao->fotos = $this->getFotos($publicacao->id);
                $publicacao->foto = @$publicacao->fotos[0]->path;
            }
            
            return $publicacoes;
        }

        return null;
    }
    
}