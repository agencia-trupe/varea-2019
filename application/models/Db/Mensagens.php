<?php

class Application_Model_Db_Mensagens extends ZendPlugin_Db_Table
{
    protected $_name = "mensagens";
    protected $_autoUtf8 = true;

    public function getMensagem($id,$force=false)
    {
    	$msg = $this->fetchRow('id = "'.$id.'" '.(!$force?' and status_id = 1':''));
    	return $msg ? $msg->body : '';
    }

}
