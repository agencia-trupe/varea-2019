<?php

class Application_Model_Db_CursosFotos extends Zend_Db_Table
{
    protected $_name = "cursos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Cursos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Cursos' => array(
            'columns' => 'curso_id',
            'refTableClass' => 'Application_Model_Db_Cursos',
            'refColumns'    => 'id'
        )
    );
}
