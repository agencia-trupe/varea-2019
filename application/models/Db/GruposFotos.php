<?php

class Application_Model_Db_GruposFotos extends Zend_Db_Table
{
    protected $_name = "grupos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Grupos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Grupos' => array(
            'columns' => 'grupo_id',
            'refTableClass' => 'Application_Model_Db_Grupos',
            'refColumns'    => 'id'
        )
    );
}
