<?php

class Application_Model_Db_Professores extends ZendPlugin_Db_Table 
{
    protected $_name = "professores";
    protected $_foto_join_table = 'professores_fotos'; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = 'professor_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
}