<?php

class Application_Model_Db_Cursos extends ZendPlugin_Db_Table 
{
    protected $_name = "cursos";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos do curso
     *
     * @param int $id - id do curso
     *
     * @return array - rowset com fotos do curso
     */
    public function getFotos($id)
    {
        if(!$curso = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($curso_fotos = $curso->findDependentRowset('Application_Model_Db_CursosFotos')){
            foreach($curso_fotos as $curso_foto){
                $fotos[] = Is_Array::utf8DbRow($curso_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    public function getLast($id=null)
    {
        // $curso = $this->getLasts(1,'data',$id ? 'id = "'.$id.'"' : '');
        $curso = $this->getLasts(1,'data desc',$id ? 'id = "'.$id.'"' : '');
        return $curso[0];
    }

    // public function getLasts($limit=3,$order='data',$_where=null)
    public function getLasts($limit=3,$order='data desc',$_where=null)
    {
        // $where = 'status_id=1 and data >= date(now()) ';
        $where = 'status_id=1 ';
        if($_where) $where.= 'and '.$_where;
        $_cursos = $this->fetchAll($where,$order,$limit);
        
        if(count($_cursos)){
            $cursos = Is_Array::utf8DbResult($_cursos);
            
            foreach($cursos as &$curso){
                $curso->fotos = $this->getFotos($curso->id);
                $curso->foto = @$curso->fotos[0]->path;
            }
            
            return $cursos;
        }

        return null;
    }

    /**
     * Adiciona dados de professores ao rowset
     */
    public function parseConsultores($rows)
    {
        $ids = array('0');
        foreach($rows as $row) {
            if((bool)$row->professor_id) $ids[] = $row->professor_id;
            $row->professor = null;
        }
        if(!count($ids)) return $rows;

        $ids = array_unique($ids);
        $insts = $this->q('select i.*, f.path as foto_path '.
            'from professores i '.
            'left join professores_fotos i_f on i_f.professor_id = i.id '.
            'left join fotos f on f.id = i_f.foto_id '.
            'where i.id in ('.implode(',', $ids).')');
        
        foreach($rows as $row)
            foreach($insts as $inst)
                if($row->professor_id == $inst->id) $row->professor = $inst;

        return $rows;
    }
    public function parseInstrutores($rows){ return $this->parseConsultores($rows); }
    public function parseProfessores($rows){ return $this->parseConsultores($rows); }
    
}