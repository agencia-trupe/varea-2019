<?php

class Application_Model_Db_Mailling extends ZendPlugin_Db_Table {
    protected $_name = "mailling";

    public function insertUpdate($data)
    {
    	$intos=array(); $values=array();
    	$intos2=array(); $values2=array();
    	$db=$this->getDefaultAdapter();
		
		foreach($data as $k => $v){
			$v = trim($v)=='' || $v=='__none__' ?
				 'null' :
				 $db->quote($v);
			
			array_push($intos,$k);
			array_push($values,$v);
			
			if($k=='data_cad') $k='data_edit';
			array_push($intos2,$k);
			array_push($values2,$v);
		}
		
		$intos = array_unique($intos);
		$intos2 = array_unique($intos2);

		$updates = array();
		for($i=0; $i<sizeof($intos2); $i++) $updates[]= $intos2[$i].'='.$values2[$i];
		
		$insert = "insert into ".$this->_name." ".
				  "(".implode(', ',$intos).") ".
				  "values (".implode(", ",$values).") ".
				  "on duplicate key update ".implode(', ', $updates);
		
		return $db->query($insert);
    }
}