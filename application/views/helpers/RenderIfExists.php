<?php
/**
 * Verifica se uma view existe antes de renderizar
 * Auxiliar da Camada de Visualização
 * @author Patrick M. Oliveira
 * 
 */
class Zend_View_Helper_RenderIfExists extends Zend_View_Helper_Abstract
{
    protected $_file = null;
    const INVALID_FILE = 'Invalid file parameter';
    
    /**
     * Método Principal
     * 
     * @param string $file - caminho da view a ser verificada
     * @return string
     */
    public function renderIfExists($file)
    {
        $this->_file = $file;
        if(!is_string($file) || empty($file)) throw new Zend_View_Exception(self::INVALID_FILE);
        if(false === $this->_fileExists()) return '';
        return $this->view->render($file);
    }

    /**
    * Check to see if a view script exists
    *
    * @return boolean
    */
    protected function _fileExists()
    {
        $paths = $this->view->getScriptPaths();
        foreach($paths as $path) if(file_exists($path . $this->_file)) return true;
        return false;
    }

}