<?php

class Application_Form_Publicacoes extends ZendPlugin_Form
{

    public function init()
    {
        $this->setMethod('post')->setAction(URL.'/publicacoes/enviar')->setAttrib('id','frm-publicacoes')->setAttrib('name','frm-publicacoes');
		
		$this->addElement('text','nome',array('label'=>'* Nome:','class'=>'txt'));   
		$this->addElement('text','email',array('label'=>'* Email:','class'=>'txt','validator'=>'EmailAddress'));
		$this->addElement('hidden','id');
        
        foreach ($this->getElements() as $elm) {
            // _d($elm->getType(),false);
            // if($elm->getType()!='Zend_Form_Element_Hidden'){
                $elm->setRequired()
                    ->addFilter('StripTags')
                    ->addFilter('StringTrim')
                    ->setAttrib('data-validate',true)
                    ->setAttrib('data-errmsg', 'Campo '.$elm->getLabel().' inválido');

                switch ($elm->getId()) {
                    case 'email':
                        $elm->addValidator('EmailAddress',false,array('token'=>'email', 'messages'=>'E-mail inválido'));
                        break;
                }
            // }
        }

        $this->removeDecs();
    }

}

