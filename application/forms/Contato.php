<?php

class Application_Form_Contato extends ZendPlugin_Form
{
    protected $mensagem_id = 1;

    public function __construct($mensagem_id=null)
    {
        if($mensagem_id) $this->mensagem_id = $mensagem_id;
        return parent::__construct();
    }

    public function init()
    {
        // configurações do form
        $this->setMethod('post')
             ->setAction(URL.'/contato/enviar/')
             ->setAttrib('id','frm-fale-conosco')
             ->setAttrib('name','frm-fale-conosco');
        
        switch ($this->mensagem_id) {
            case 5:
                $this->addElement('text','tema',array('label'=>'roteiro','class'=>'txt2'));
                $this->addElement('hidden','tema_id');
                break;
        }

        // elementos
        $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
        $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
        $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
        // $this->addElement('text','assunto',array('label'=>'assunto','class'=>'txt2'));
        
        switch ($this->mensagem_id) {
            case 5:
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                
                $this->getElement('mensagem')//->setRequired()
                     ->addFilter('StripTags')
                     ->addFilter('StringTrim');
                break;
            
            case 3:
                $this->addElement('text','data_interesse',array('label'=>'data do evento','class'=>'txt2'));
                $this->addElement('text','participantes',array('label'=>'participantes','class'=>'txt2'));
                $this->addElement('text','empresa',array('label'=>'escola','class'=>'txt2'));
                break;
            
            case 2:
                $this->addElement('text','data_interesse',array('label'=>'data do evento','class'=>'txt2'));
                $this->addElement('text','participantes',array('label'=>'participantes','class'=>'txt2'));
                break;
            
            case 1:
                $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
                
                $this->getElement('mensagem')//->setRequired()
                     ->addFilter('StripTags')
                     ->addFilter('StringTrim');
                break;
        }

        // filtros / validações
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('email')->setRequired()
             ->addValidator('EmailAddress')
             ->addFilter('StripTags');
        
        // remove decoradores
        $this->removeDecs();
    }

    public function changeClasses($class='txt',$disable=false)
    {
        foreach ($this->getElements() as $elm)
            if($elm->getType()!='Zend_Form_Element_Radio'){
                $elm->setAttrib('class',$class);
                if($disable) $elm->setAttrib('disabled',true);
            }
        return $this;
    }

    public function setAdminParams()
    {
        if($this->getElement('tema1')){
            $this->removeElement('tema');
            $this->addElement('text','tema',array('label'=>'tema','class'=>'txt2'));
        }

        if($this->getElement('cidade')){
            $this->removeElement('cidade');
            $this->addElement('text','cidade',array('label'=>'região','class'=>'txt2'));
        }

        // if($this->getElement('assunto')){
        //     $this->removeElement('assunto');
        //     $this->addElement('text','assunto',array('label'=>'assunto','class'=>'txt2'));
        // }

        if($this->getElement('certificacao')){
            $this->removeElement('certificacao');
            $this->addElement('text','certificacao',array('label'=>'certificação','class'=>'txt2'));
        }

        if($this->getElement('horario')){
            $this->removeElement('horario');
            $this->addElement('text','horario',array('label'=>'horário','class'=>'txt2'));
        }

        if($this->getElement('participantes')){
            $this->getElement('participantes')->setLabel('n. de participantes');
        }

        $this->changeClasses('txt disabled',true);
    }


}

