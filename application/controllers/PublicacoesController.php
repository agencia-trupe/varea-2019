<?php

class PublicacoesController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->view->titulo = 'Publicações';
    	$this->_url = $this->view->url = URL.'/publicacoes';
        $this->publicacoes = new Application_Model_Db_Publicacoes();
    }

    public function indexAction()
    {
        $publicacoes = _utfRows($this->publicacoes->fetchAll(
            'status_id=1',
            array('ordem')
        ));

        $this->view->publicacoes = $publicacoes;
    }

    public function internaAction()
    {
        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('publicacoes');
        // if($alias=='enviar') return $this->enviarAction();
        // if($alias=='enviar.json') return $this->enviarAction();
    	$alias = explode('-', $alias);
    	$id = (int)array_pop($alias);
    	$alias = implode('-', $alias);
        
        $publicacao = _utfRow($this->publicacoes->fetchRow(
        	'status_id=1 and id = '.$id
        ));
        if(!$publicacao) return $this->_redirect('publicacoes');
    	if($publicacao->alias!=$alias) return $this->_redirect('publicacoes');
        $this->view->publicacao = $publicacao;
    	// _d(array($id,$alias,$publicacao));
    }

    public function enviarAction()
    {
        // exit(json_encode(array('msg'=>'OK'))); // test
        $id = ($this->_hasParam('id')) ? (int)$this->_getParam('id') : null;
        if(!$id) exit(json_encode(array('error'=>'Arquivo não encontrado')));
        
        $this->publicacoes = new Application_Model_Db_Publicacoes();
        $this->publicacoesdl = new Application_Model_Db_PublicacoesDownload();
        $this->mailling = new Application_Model_Db_Mailling();
        $row = _utfRow($this->publicacoes->get($id));
        if(!$row) exit(json_encode(array('error'=>'Arquivo não encontrado')));
        
        $post = $this->_request->getPost();
        $form = new Application_Form_Publicacoes();

        // validar email ao enviar form
        if(!$form->isValid($post)){
            exit(json_encode(array('error'=>'Preencha os campos corretamente')));
        }
        
        try {
            // salvar dl do email em tabela separada por publicacao
            // salvar ip + nome + email + datahora + publicacao_id
            $data = $post;
            $data['data_cad'] = date('Y-m-d H:i:s');
            $data['ip'] = Is_Server::getIp();
            $data['publicacao_id'] = $data['id'];
            unset($data['id']);
            // return $data;
            
            // se já baixou exibir mensagem
            $data_compare = array(
                'email'=>$data['email'],
                'publicacao_id'=>$data['publicacao_id']
            );
            $dlExists = $this->publicacoesdl->exists($data_compare);
            // if($dlExists)
                // exit(json_encode(array('msg'=>'<span class="status error">* Você já baixou este item *</span><br><br>'.nl2br($row->body))));

            // salva dl
            if(!$dlExists) $insertId = $this->publicacoesdl->insert($data);
            
            // salvar em mailling ?
            $data_m = array(
                'nome' => $data['nome'],
                'email' => $data['email'],
                'data_cad' => $data['data_cad'],
                'arquivo_id' => $data['publicacao_id'],
                'tipo' => 3,
                'area' => 3,
            );
            if(!$dlExists) $this->mailling->insert($data_m);
            
            // enviar link por email para baixar
            // $sub = SITE_TITLE.' - Download de Publicação';
            $sub = 'Download de Publicação';
            $html = nl2br($row->body2);
            $html.= '<br><br>';
            $html.= '<a href="'.FILE_URL.'/publicacoes/'.$row->path.'">Baixar Arquivo &raquo;</a>';
            $html.= '<br><br>';
            
            // if(!$dlExists) try { Trupe_Institutoph2_Mail::send(
            try { Trupe_Institutoph2_Mail::send(
                $post['email'],
                $post['nome'],
                $sub,
                $html
            ); } catch(Exception $e){ }
            
            exit(json_encode(array(
                'msg'=>nl2br($row->body),
                'file'=>FILE_URL.'/publicacoes/'.$row->path,
            )));
        } catch (Exception $e) {
            $err = 'Erro ao enviar, tente novamente.';
            if(ENV_DEV) $err.= '<br>'.$e->getMessage();
            exit(json_encode(array('error'=>$err)));
        }
    }


}

