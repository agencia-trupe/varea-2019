<?php

class CursosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->titulo = 'Cursos e Palestras';
        $this->cursos = new Application_Model_Db_Cursos();
    }

    public function indexAction()
    {
        $rows = $this->cursos->getLasts(20);
        $rows = $this->cursos->parseProfessores($rows);
        $this->view->rows = $rows;
        // _d($rows);
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('cursos');
    	$alias = explode('-', $alias);
    	$id = (int)array_pop($alias);
    	$alias = implode('-', $alias);
    	// _d(array($id,$alias));

        $row = $this->cursos->getLast($id);
    	if(!$row) return $this->_redirect('cursos');
    	if($row->alias!=$alias) return $this->_redirect('cursos');
        $row = reset($this->cursos->parseProfessores(array($row)));
        $this->view->row = $row;
        // _d($row);
    }


}

