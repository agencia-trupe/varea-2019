<?php

class IndexController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->banners = new Application_Model_Db_Destaques();
        $this->cursos = new Application_Model_Db_Cursos();
    }

    public function indexAction()
    {
        $banners = $this->banners->getLasts(6,'ordem');
        // array_pop($banners);
        $this->view->banners = $banners;
        // _d($banners);

        $curso = $this->cursos->getLast();
        $curso = reset($this->cursos->parseProfessores(array($curso)));
        $this->view->curso = $curso;
        // _d($curso);
    }

    public function newsletterAction()
    {
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            // $post = $post['newsletter'];
            $post = $post;
            $validator = new Zend_Validate_EmailAddress();
            
            if(trim($post['nome']) == '' || !$validator->isValid($post['email'])){
                return array("error"=>1,"message"=>"* Preencha os campos corretamente","data"=>$post);
            } else {
                try {
                    // $data = $post;
                    $data = array_map('utf8_decode',$post);
                    $data['data_cad'] = date('Y-m-d H:i:s');
                    //unset($data['submit']);
                    $table = new Application_Model_Db_Mailling();
                    
                    $html = '<h1 style="font-size:14px">Cadastro Newsletter</h1><p style="font-size:11px">'.
                        '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '</p>';
                    
                    try { Trupe_Varea_Mail::sendWithReply(
                        $post['email'],
                        $post['nome'],
                        'Cadastro Newsletter',
                        $html
                    ); } catch(Exception $e){ }
                    
                    $html2= '<p style="font-size:11px">'.
                        'Cadastro em Newsletter realizado com sucesso!<br /><br />'.
                        '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '</p>';
                    
                    try { Trupe_Varea_Mail::send(
                        $post['email'],
                        $post['nome'],
                        'Confirmação de cadastro em Newsletter',
                        $html2
                    ); } catch(Exception $e){ }
                    
                    $table->insert($data);
                    return array("message"=>"Cadastro efetuado com sucesso!");
                    $form->reset();
                } catch(Exception $e){
                    // if(strstr($e->getMessage(),'uplicate')) return array('message'=>'* Você já está cadastrado.');
                    if(strstr($e->getMessage(),'uplicate')) return array('message'=>'Cadastro efetuado com sucesso!');
                    return array("error"=>1,"message"=>'* Erro ao enviar formulário');
                }
            }
        }
    }

    public function mailAction()
    {
        try{
            Trupe_Varea_Mail::send('patrick.trupe@gmail.com','Trupe','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }


}