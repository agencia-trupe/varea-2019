<?php

class GruposController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->titulo = 'Grupos de Estudos';
        $this->grupos = new Application_Model_Db_Grupos();
    }

    public function indexAction()
    {
        $rows = $this->grupos->getLasts(20);
        $rows = $this->grupos->parseProfessores($rows);
        $this->view->rows = $rows;
        // _d($rows);
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('grupos');
    	$alias = explode('-', $alias);
    	$id = (int)array_pop($alias);
    	$alias = implode('-', $alias);
    	// _d(array($id,$alias));

        $row = $this->grupos->getLast($id);
    	if(!$row) return $this->_redirect('grupos');
    	if($row->alias!=$alias) return $this->_redirect('grupos');
        $row = reset($this->grupos->parseProfessores(array($row)));
        $this->view->row = $row;
        // _d($row);
    }


}

