<?php

class ProfessoresController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->titulo = 'Professores e Colaboradores';
        $this->professores = new Application_Model_Db_Professores();
    }

    public function indexAction()
    {
        $professores = $this->professores->fetchAllWithFoto(
        	'status_id=1',
        	'titulo'
        );
        $this->view->professores = $professores;
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('professores');
    	$alias = explode('-', $alias);
    	$id = (int)array_pop($alias);
    	$alias = implode('-', $alias);
        
        $professor = $this->professores->fetchRowWithFoto(
        	'status_id=1 and t1.id = '.$id,
        	'titulo'
        );
        if(!$professor) return $this->_redirect('professores');
    	if($professor->alias!=$alias) return $this->_redirect('professores');
        $this->view->professor = $professor;
    	// _d(array($id,$alias,$professor));

    	// $this->view->titulo = $professor->titulo;
    }


}

