<?php

class QuemSomosController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(2);
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->titulo;
        // _d($pagina);
    }


}

