<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{
    protected $mensagem_id = 1;

    public function init()
    {
        /* Initialize action controller here */
        $this->view->meta_description = 'Contato '.SITE_TITLE;
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        $this->view->titulo = 'Contato';
    }
    
    public function enviarAction()
    {
        // return array('msg'=>'OK'); // test
        $r = $this->getRequest();

        if(!$this->isAjax() && !$r->isPost()){
            $this->messenger->addMessage('Requisição inválida','error');
            return $this->_redirect('contato');
        }
        if(!$r->isPost()) return array('error'=>'Requisição inválida');
        
        // $this->mailling = new Application_Model_Db_Mailling();
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->mensagens_contato = new Application_Model_Db_MensagensContato();
        $this->mensagem = $this->mensagens->get($this->mensagem_id);

        $form = new Application_Form_Contato();
        $post = $r->getPost();
        $assunto = (bool)trim($r->getParam('assunto')) ? ' - '.trim($r->getParam('assunto')) : $this->mensagem->subject;
        
        if($form->isValid($post)){ // valida post
            $html = "<h1>".$assunto."</h1>". // monta html
                    nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                    "<b>E-mail:</b> <a href='mailto:".
                    $r->getParam('email')."'>".$r->getParam('email').
                    "</a><br/>".
                    "<b>Telefone:</b> ".$r->getParam('telefone');//."<br/>".
                    // "<b>Celular:</b> ".$r->getParam('celular');
            
            $data_mensagem_contato = array(
                'nome'     => $post['nome'],
                // 'empresa'  => $post['empresa'],
                'email'    => $post['email'],
                'telefone' => Is_Cpf::clean($post['telefone']),
                // 'cpf'      => Is_Cpf::clean($post['cpf']),
                'assunto'  => $assunto,
                'mensagem' => $post['mensagem'],
                'data_cad' => date('Y-m-d H:i:s'),
                'mensagem_id' => $this->mensagem_id,
            );

            try {
                // salva mensagem de contato
                $this->mensagens_contato->insert($data_mensagem_contato);

                // salvar em mailling (insertUpdate on duplicate)
                // $this->mailling->insertUpdate(array(
                //     'nome' => $post['nome'],
                //     'email' => $post['email'],
                //     // 'telefone' => Is_Cpf::clean($post['telefone']),
                //     'data_cad' => date('Y-m-d H:i:s'),
                // ));

                // tenta enviar o e-mail
                try { Trupe_Varea_Mail::sendWithReply(
                    $post['email'],
                    $post['nome'],
                    ''.$assunto,
                    $html
                ); } catch(Exception $e){ }
                return array('msg'=>'Mensagem enviada com sucesso!');
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>'* Preencha todos os campos');
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}