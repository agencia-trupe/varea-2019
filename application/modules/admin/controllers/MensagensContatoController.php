<?php

class Admin_MensagensContatoController extends ZendPlugin_Controller_Ajax
{
    protected $_csv_write_header = false;

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "MENSAGENS CONTATO";
        $this->view->section = $this->section = "mensagens-contato";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        
        // models
        $this->mensagens_contato = new Application_Model_Db_MensagensContato();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));

        $this->view->table_config = array('nome'=>'Nome','email'=>'E-mail','telefone'=>'Telefone');
        
        Admin_Model_Login::setControllerPermissions($this,$this->section);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 50;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%' ";
            // $where.= 'and status_id = 1 ';
            if((bool)trim($this->_getParam('data_from'))) $where.= "and data_cad >= '".Is_Date::br2am($this->_getParam('data_from'))." 00:00:00' ";
            if((bool)trim($this->_getParam('data_to')))   $where.= "and data_cad <= '".Is_Date::br2am($this->_getParam('data_to'))." 23:59:59' ";
            if((bool)trim($this->_getParam('search-treinamento'))) $where.= "and tema_id = '".$this->_getParam('search-treinamento')."' ";
            $rows = $this->mensagens_contato->fetchAll($where,'id desc',$limit,$offset);
            
            $total = $this->view->total = $this->mensagens_contato->count($where);
        } else {
            $rows = $this->mensagens_contato->fetchAll(null,'id desc',$limit,$offset);
            $total = $this->view->total = $this->mensagens_contato->count();
        }

        if($rows){
            $rows = $this->mensagens_contato->getMensagens($rows);
            // $rows = $this->mensagens_contato->getArquivos($rows);
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        $this->view->page = $pagination->page;
        
        // adicionando assunto no titulo se busca por msg
        if($this->_getParam('search-by')=='mensagem_id'){
            $msg = $this->mensagens_contato->s1('mensagens','id,titulo','id="'.$this->_getParam('search-txt').'"');
            if($msg) $this->view->titulo = "MENSAGENS &rarr; ".$msg->titulo;
            $this->view->search_mensagem = true;
        }
        
        // ações para registro de interesse em treinamentos
        // if($this->_getParam('search-by')=='mensagem_id' && $this->_getParam('search-txt')=='13'){
        if($this->_getParam('search-by')=='mensagem_id' && in_array($this->_getParam('search-txt'),array(13,22))){
            $this->view->search_treinamentos = true;
            $this->view->table_config = array('treinamento_titulo'=>'Tema','nome'=>'Nome','email'=>'E-mail','telefone'=>'Telefone');
            
            $this->treinamentos = new Application_Model_Db_Treinamentos();
            $this->view->treinamentosKV = $this->treinamentos->getKeyValues('titulo',array('__none__'=>'Tema...'));

            if((bool)$rows) {
                $_treinamentos = array(); $_tids = array();

                foreach ($rows as $row) if((bool)$row->tema_id) $_tids[] = $row->tema_id;
                $_tids = (count($_tids)) ? array_unique($_tids) : array('0');

                $treinamentos = Is_Array::utf8DbResult($this->treinamentos->fetchAll(
                    'id in ('.implode(',', $_tids).')'
                ));

                foreach ($treinamentos as $trei) $_treinamentos[$trei->id] = $trei;
                foreach ($rows as &$row) 
                    if(array_key_exists($row->tema_id, $_treinamentos)) {
                        $row->treinamento_titulo = $_treinamentos[$row->tema_id]->titulo;
                        $row->_treinamento = $_treinamentos[$row->tema_id];
                    }
            }
        }
        
        $form = new Admin_Form_MensagensContato();
        $this->view->mensagens = $form->getElement('mensagem_id')->options;
        $this->view->rows = $rows;

        // configure for export (csv,pdf,etc)
        if($this->_hasParam('export')){
            $_rows = array();
            foreach($this->view->rows as $row){
                $_row = array();
                foreach($this->view->table_config as $k => $v)
                    if(!in_array($k, array('id','export','ver')))
                        $_row[] = utf8_decode($row->{$k});
                $_rows[] = $_row;
            }

            exit(print $this->_toCsv($_rows, $this->section.'-'.date('YmdHis').'.csv'));
        }
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; VISUALIZAR":" &rarr; INSERIR");
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $data['telefone'] = Is_Format::tel($data['telefone'],'cel');
            $data['cpf'] = Is_Cpf::format($data['cpf']);
            $data['cnpj'] = Is_Cpf::formatCnpj($data['cnpj']);
            $form = new Application_Form_Contato($data['mensagem_id']);
            $this->view->id = $this->mensagem_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            // $this->view->fotos = $this->fotosAction();
            // $this->view->allow_photos = $data['allow_photos'];

            $row = (object)$data;
            $row = $this->mensagens_contato->getMensagem($row);
            $row = $this->mensagens_contato->getArquivo($row);
            $this->view->row = $row;

            // $form->setAdminParams();
            $form->changeClasses('txt',1);
        } else {
            $form = new Application_Form_Contato();
            return $this->_redirect('admin/mensagens-contato');
            // $form->removeElement('body');
            //$form->removeElement('status_id');
            $data = array();//'status_id'=>'1','allow_photos'=>'1');
        }

        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->mensagens_contato->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            // $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            // $data['body']  = isset($data['body']) ? strip_tags($data['body'],'<b><a><i><u><br><ul><ol><li><img><p><div>') : null;
            $data['body']  = isset($data['body']) ? cleanHtml($data['body']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            // $data['allow_photos'] = 1;
            $data = array_map('utf8_decode',$data);
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->mensagens_contato->update($data,'id='.$id) : $id = $this->mensagens_contato->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->mensagens_contato->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        // $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
        $this->_forward('new',null,null,array('data'=>(array)$row));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->mensagens_contato->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }

    public function delSelectAction()
    {
        $ids = addslashes($this->_getParam("delid"));
        $page = addslashes($this->_getParam("delpage"));
        $url_redirect = 'admin/'.$this->section;
        if((int)$page>1) $url_redirect.= '?page='.$page;
        
        if(trim($ids)=='') {
            $this->messenger->addMessage('Nenhum registro para excluir, tente novamente','error');
            return $this->_redirect($url_redirect);
        }
        
        try {
            $this->mensagens_contato->delete("id in (".$ids.")");
            $this->messenger->addMessage('Registros excluídos com sucesso');
        } catch(Exception $e) {
            $err = 'Erro ao excluir, tente novamente';
            if(APPLICATION_ENV=='development') $err.= '<br>'.$e->getMessage();
            $this->messenger->addMessage($err,'error');
        }
        
        return $this->_redirect($url_redirect);
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('mensagens_contato_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->mensagem_id)){
            $select->where('f2.mensagem_id = ?',$this->mensagem_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/mensagens-contato/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $mensagens_contato_fotos = new Application_Model_Db_MensagensContatoFotos();
            $mensagem_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $mensagens_contato_fotos->insert(array("foto_id"=>$foto_id,"mensagem_id"=>$mensagem_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}