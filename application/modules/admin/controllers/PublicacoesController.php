<?php

class Admin_PublicacoesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "Publicações";
        $this->view->section = $this->section = "publicacoes";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;

        Admin_Model_Login::checkAuth($this,$this->section) ||
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        
        $this->view->MAX_FOTOS = 1;
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));
        
        // models
        $this->publicacoes = new Application_Model_Db_Publicacoes();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        // ini_set('memory_limit', '512M');
        if($this->_hasParam('dump')) _d(array(
            'max_size' => $this->view->MAX_SIZE,
            'memory'   => ini_get('memory_limit'),
        ));

        Admin_Model_Login::setControllerPermissions($this,$this->section);
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 1000;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->publicacoes->fetchAll($where,'ordem',$limit,$offset);
            
            $total = $this->view->total = $this->publicacoes->count($where);
        } else {
            $rows = $this->publicacoes->fetchAll(null,'ordem',$limit,$offset);
            $total = $this->view->total = $this->publicacoes->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Publicacoes();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->publicacao_id = $data['id'];
            $form->addElement('hidden','id');
            $this->view->row = (object)$data;
            //$form->removeElement('allow_photos');
            // $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = 0;//$data['allow_photos'];
        } else {
            // $form->removeElement('body');
            // $form->removeElement('url');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->publicacoes->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            $data['body']  = isset($data['body']) ? strip_tags($data['body']) : null;
            $data['body2']  = isset($data['body2']) ? strip_tags($data['body2']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            if(!$row) $data['ordem'] = $this->publicacoes->getNextOrdem();
            $data = array_map('utf8_decode',$data);
            
            $file = null;
            if((bool)@$_FILES['file'] && (bool)$_FILES['file']['name'])
                $file = $_FILES['file'];
            // _d($file);

            // remove dados desnecessários
            $unsets = 'submit,module,controller,action,MAX_FILE_SIZE,file';
            foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
            // _d($data);

            // upload do arquivo
            if($file){
                $path = $this->file_path;
                $maxsz= ini_get('upload_max_filesize').'B'; //50mb;
                $ext = 'pdf,doc,docx,xls,xlsx,ppt,pptx,odt,rtf,txt,'.
                       // 'jpg,jpeg,png,bmp,gif,tiff,'.
                       'zip,rar,7zip';
                $error = 'Arquivo inválido';
                // _d($path);
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload//->addValidator('Size', false, array('max' => $maxsz))
                       // ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,'file')
                       ->setDestination($path);

                if($upload->isValid('file')){
                    $upload->receive('file');

                    $data['path'] = $rename;
                    // if(!trim($data['descricao'])) $data['descricao'] = $file['name'];
                    // verficando ext do nome do arquivo com do arquivo
                    // $ext1 = Is_File::getExt($rename);
                    // $ext2 = Is_File::getExt($data['descricao']);
                    // if($ext1!=$ext2) $data['descricao'].= '.'.$ext1;
                    
                    // se edit e já tem arquivo, apaga anterior
                    if($row) if((bool)trim($row->path)) if(file_exists($path.'/'.$row->path))
                        unlink($path.'/'.$row->path);
                } else {
                    if(ENV_DEV) _d($upload->getErrors());
                    $err = $error;
                    if(APPLICATION_ENV!='production1') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    // _d($err);
                    
                    $this->_redirect('admin/publicacoes/'.(($row)?'edit/'.$id:'new'));
                }
            }
            
            ($row) ? $this->publicacoes->update($data,'id='.$id) : $id = $this->publicacoes->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            // $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
            $this->_redirect(URL.'/admin/publicacoes/'.($row ? 'edit/'.$row->id : 'new' ));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->publicacoes->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $row = $this->publicacoes->get($id);
            if($row) if((bool)trim(@$row->path)) unlink($this->file_path.'/'.$row->path);
            $this->publicacoes->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('publicacoes_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->publicacao_id)){
            $select->where('f2.publicacao_id = ?',$this->publicacao_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);

        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            
            $arr = array();
        } catch(Exception $e) {
            $arr = array("erro"=>$e->getMessage());
        }

        $fotos->delete("id=".(int)$id);
        return $arr;
    }
    
    public function uploadAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        // $max_size = '5120'; // '2048'
        $max_size = $this->view->MAX_SIZE.'MB'; //'5120'; //'2048';
        $ext = 'jpeg,jpg,png,gif,bmp';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/publicacoes/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, $ext)
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida ('.$ext.') de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('2000','2000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $publicacoes_fotos = new Application_Model_Db_PublicacoesFotos();
            $publicacao_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $publicacoes_fotos->insert(array("foto_id"=>$foto_id,"publicacao_id"=>$publicacao_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->publicacoes->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}

