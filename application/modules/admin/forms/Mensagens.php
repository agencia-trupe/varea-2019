<?php

class Admin_Form_Mensagens extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/mensagens/save')
             ->setAttrib('id','frm-mensagens')
             ->setAttrib('name','frm-mensagens');
        
        // elementos
        $this->addElement('hidden','titulo',array('class'=>'txt'));
        $this->addElement('text','subject',array('label'=>'Assunto','class'=>'txt'));
        $this->addElement('text','email',array('label'=>'E-mail de destino','class'=>'txt'));
        $this->addElement('textarea','body',array('label'=>'Conteúdo','class'=>'txt wysiwyg'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('subject')->setRequired();
        $this->getElement('email')->setRequired()->addValidator('EmailAddress')
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        
        // remove decoradores
        $this->removeDecs();
    }
}

