<?php

class Admin_Form_Cursos extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/cursos/save/')
             ->setAttrib('id','frm-cursos')
             ->setAttrib('name','frm-cursos');

        $insts = new Application_Model_Db_Professores();
        $professores = $insts->kv();
        $professores = array_map('utf8_decode', $professores);
        // _d($professores);
        
        // elementos
        $this->addElement('text','data',array('label'=>'Data','class'=>'txt txt3 mask-date'));
        $this->addElement('text','horario',array('label'=>'Horário','class'=>'txt'));
        $this->addElement('select','professor_id',array('label'=>'Professor','class'=>'txt','multiOptions'=>$professores));
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        // $this->addElement('text','url',array('label'=>'Vídeo URL','class'=>'txt'));
        $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        $this->addElement('textarea','body2',array('label'=>'Objetivos','class'=>'txt wysiwyg'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        $this->getElement('body2')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('data')->setRequired();
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

