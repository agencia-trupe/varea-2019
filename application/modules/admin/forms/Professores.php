<?php

class Admin_Form_Professores extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/professores/save/')
             ->setAttrib('id','frm-professores')
             ->setAttrib('name','frm-professores');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Nome','class'=>'txt'));
        // $this->addElement('hidden','alias');
        // $this->addElement('text','cargo',array('label'=>'Área','class'=>'txt'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','body',array('label'=>'Texto','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','body2',array('label'=>'Texto oculto','class'=>'txt'));
        // $this->addElement('textarea','depoimento',array('label'=>'Depoimento','class'=>'txt'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

