<?php

class Admin_Form_Publicacoes extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/publicacoes/save/')
             ->setAttrib('id','frm-publicacoes')
             ->setAttrib('name','frm-publicacoes');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        $this->addElement('text','autor',array('label'=>'Autores','class'=>'txt'));
        // $this->addElement('text','url',array('label'=>'Vídeo URL','class'=>'txt'));
        $this->addElement('file','file',array('label'=>'Arquivo','class'=>'txt'));

        $this->addElement('textarea','body',array('label'=>'Texto confirmação (site)','class'=>'txt'));
        $this->addElement('textarea','body2',array('label'=>'Texto confirmação (e-mail)','class'=>'txt'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','body',array('label'=>'Descrição','class'=>'txt wysiwyg'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',10)->setAttrib('cols',1);
        $this->getElement('body2')->setAttrib('rows',10)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

