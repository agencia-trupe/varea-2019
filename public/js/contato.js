$(document).ready(function(){
    _prevalues();
    
    $('input#telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // enviando contato
    var $formContato = $('#form-contato');
    $('a.submit',$formContato).click(function(){
        $formContato.submit();
    });
    $formContato.submit(function(e){
        e.preventDefault();
        var $status = $('p.status',$formContato).removeClass('error');
        $status.html('Enviando...');
        
        head.js(JS_PATH+'/is.simple.validate.js',function(){
            if($formContato.isSimpleValidate()){
                var url  = URL+'/'+CONTROLLER+'/enviar.json',
                    data = $formContato.serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm('form.contato');
                    /*Shop.messageBox('<p class="shop-message-title contato">'+json.msg+'</p>'+
                                    '<p class="shop-message-buttons">'+
                                    '<a href="#" class="shop-close-message-box fechar-contato single">Fechar</a>'+
                                    '</p>');*/
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
        });

        return false;
    });
});

function _prevalues(v){
    $('.prevalue').each(function(){
        var $t = $(this);
        $t.prevalue(v || $t.data('prevalue'));
    });
}
