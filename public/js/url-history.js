var hasPush = supports_history_api(), isPushed = false;

var filters = $('#top ul.navigation > li > a, #link-banner-next'),
	isFiltered = false,
	hash = '#', hash1 = '';

filters.click(function(e){
	e.preventDefault();
	var $this = $(this);
	hash1= $this.attr('href').split('/').reverse().shift();
	hash = '#'+(hash1.length ? hash1 : 'slideshow-wrapper');
	// console.log(hash); return;
	
	filters.removeClass('active');
	$this.addClass('active');

	_show(hash);

	filters.each(function(i,elm){
		if(elm.href == $this.attr('href'))
			$(elm).addClass('active');
	});
	
	return false;
});

// filtro inicial
if(location.href != URL+'/' && 1){
	var filter = location.href.split('/').reverse().shift();

	// _show('#main-index-'+filter);
	// filters.filter('.empresa-'+filter).addClass('active');
	window.setTimeout(function(){
		_show('#'+filter);
		// console.log('#'+filter);
	},(APPLICATION_ENV=='development' ? 1000 : 500));
	
	// var elm = document.getElementById('main-index-'+filter);
	// elm.scrollIntoView();
	// $(window).scrollTop(elm.offset().top).scrollLeft(elm.offset().left);
}

function _show(hash){
	var _pd = -($('#top').height()+1),
		_top = $(hash).offset().top + _pd;
	
	$('html, body').animate({
        scrollTop: _top
    }, 1000);

    if(isMobile) if($('.top-menu').hasClass('show')) {
    	_sto(function(){
    		$('#mobile-toggle').trigger('click');
    	},.2);
    }

    var filterType = 'page', psData = {};
		psData[filterType] = hash,
		hash1 = hash.replace('#','');
	if(hash1=='slideshow-wrapper') hash1 = '';
	_push(psData,filterType,hash1);
}

function supports_history_api() {
	return !!(window.history && history.pushState);
}

function _push(data,name,url){
	if(hasPush){
		history.pushState(data,name,url);
		isPushed = true;
	}
}

// marcando menu ao scroll
var mains_index = ['#slideshow-wrapper'], lastScrolled = '';
$('.main.main-index section').each(function(i,elm){
	mains_index.push('#'+elm.id);
});
// console.log(mains_index);
$(document).scroll(function(e){
	for(var mii in mains_index){
		if(isElementInView(mains_index[mii])){
			if(mains_index[mii]!=lastScrolled) {
				filters.removeClass('active');
				filters.eq(mii-1).addClass('active');
				lastScrolled = mains_index[mii];
				// console.log([mii,mains_index[mii]]);
			}
		}
	}
});

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function isElementInView(element, fullyInView) {
    var pageTop = $(window).scrollTop();
    var pageBottom = pageTop + $(window).height();
    var elementTop = $(element).offset().top;
    var elementBottom = elementTop + $(element).height();
    
    // fix for show only when a half or more is visible
    var scrollPadding = $(window).height()/2;
    elementTop+= scrollPadding;
    elementBottom+= scrollPadding;

    return (fullyInView === true) ?
        ((pageTop < elementTop) && (pageBottom > elementBottom)) :
    	((elementTop <= pageBottom) && (elementBottom >= pageTop));
}