(function ($) {
    $.fn.isSimpleValidate = function(opt){
        var $t  = $(this),
            def = {pre:false},
            o   = $.extend(def,opt),
            err = 0;
        
        $t.find('.required').each(function(){
            if(o.pre){
                if($.trim($(this).val()) == '' || $.trim($(this).val()) == $(this).data('prevalue')){
                    err++;
                }
            } else if($.trim($(this).val()) == ''){
                err++;
            }
        });
        
        console.log(err);
        return (err > 0) ? false : true;
    }
})(jQuery);