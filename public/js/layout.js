var fbLoaded = false, cycleLoaded = false, intLess,
    windowWidth, isDesktop, isMobile, isTablet, isSmartphone;

$(document).ready(function(){
    if(APPLICATION_ENV == 'development') head.js(JS_PATH+'/less.js',function(){
        less.watch();
        return;
        intLess = window.setInterval(function(){
            localStorage.clear();
            less.refresh();
        },1500);
    }); // load less in dev

    // ativa menus via javascript
    var i, j, menus_ativar = {
        'top-menu' : {
            '/professores/' : 2,
            '/cursos/' : 3,
            '/grupos/' : 4,
            '/publicacoes/' : 5
        }
    };
    for(j in menus_ativar){
        for(i in menus_ativar[j]){
            if(location.href.indexOf(i)!=-1){
                var $li = $('#'+j+' ul li').eq(menus_ativar[j][i]);
                $li.addClass('active').find('a').addClass('active');
            }
        }
    }
    if(MODULE!='admin') {
        head.js(JS_PATH+'/url-history.js');
        if(CONTROLLER!='index') head.js(JS_PATH+'/index.js');
    }

    $('#mobile-toggle').click(function(e){
        $(this).toggleClass('close');
        // $('#top-menu').toggleClass('show');
        var $topMenu = $('#top-menu');
        if($topMenu.hasClass('show')) {
            $topMenu.slideUp(function(){
                $topMenu.removeClass('show');
            });
        } else {
            $topMenu.slideDown(function(){
                $topMenu.addClass('show');
            });
        }
    });
    
    // máscaras padrões
    $("input.mask-cpf").mask("999.999.999-99");
    $("input.mask-cep").mask("99999-999");
    $("input.mask-data,input.mask-date").mask("99/99/9999");
    $("input.mask-hour").mask("99:99");
    $("input.mask-hours").mask("99:99:99");
    $("input.mask-ddd").mask("99");
    $("input.mask-tel").mask("(99)9999-9999");
    $("input.mask-cel").mask("(99)9999-9999?9");
    $("input.mask-tel-no-ddd").mask("9999-9999");
    $("input.mask-currency").maskCurrency();

    $('[autoselect]').live('focus',function(){ this.select(); });
    
    $(".prevalue").each(function(){ $(this).prevalue(); }); // prevalue em campos com a class .prevalue
    // $(".prevalue").prevalue(); // prevalue em campos com a class .prevalue
    
    $("a[href^='#']").live("click",function(e){ // remove click de links vazios (que começam por #)
        e.preventDefault();
    });
    $("a.js-back").live("click",function(e){ // adiciona ação de voltar a links com a classe js-back
        history.back();
    });
    
    if($(".pagination").size()){ // se tiver paginação
        $(".pagination .navigation.left").html("&laquo;").attr('title','Página anterior'); // arruma texto da navegação
        $(".pagination .navigation.right").html("&raquo;").attr('title','Próxima página');
        if($(".pagination a").size() <= 1){
            $(".pagination").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }
    
    if($(".paginacao").size()){ // se tiver paginação
        if($(".paginacao a").size() < 1){
            $(".paginacao").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }
    
    if(SCRIPT) head.js(JS_PATH+'/'+SCRIPT+'.js'); // carrega scripts de páginas

    // newsletter
    var $newsletterForm = $('.newsletter-form');
    $('.submit',$newsletterForm).click(function(e){
        $newsletterForm.submit();
    });
    $newsletterForm.submit(function(e){
        e.preventDefault();
        var $form = $(this), $status = $('.newsletter-form .status').removeClass('error').html('Enviando...');
        
        $.post(URL+'/index/newsletter.json',$form.serialize(),function(json){
            if(json.error) $status.addClass('error');
            $status.html(json.message);
        },'json');
    });

    // enviando contato footer
    var $formFooter = $('#form-contato-footer');
    $('.submit',$formFooter).click(function(){
        $formFooter.submit();
    });
    $formFooter.submit(function(e){
        e.preventDefault();
        var $status = $('p.status',$formFooter).removeClass('error');
        $status.html('Enviando...');
        
        head.js(JS_PATH+'/is.simple.validate.js',function(){
            if($formFooter.isSimpleValidate({pre:true})){
                var url  = $formFooter.attr('action')+'.json',
                    data = $formFooter.serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm('form.contato');
                    /*Shop.messageBox('<p class="shop-message-title contato">'+json.msg+'</p>'+
                                    '<p class="shop-message-buttons">'+
                                    '<a href="#" class="shop-close-message-box fechar-contato single">Fechar</a>'+
                                    '</p>');*/
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
        });

        return false;
    });
    
    // busca
    var b = $("#busca");
    $('#frm-busca').submit(function(e){
        e.preventDefault();
        
        if(b.val() == '' || b.val() == b.data('prevalue') || b.val() == b.attr('placeholder')){
            alert2('Escreva algum produto para buscar');
            b.focus();
        } else {
            window.location = URL+'/busca/'+b.val();
        }
        
        return false;
    });
    
    // flash-messages
    if($("#flash-messages").size() && CONTROLLER!='admin'){
        var $flash = $("#flash-messages");
        var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        $flash.css({top:y+10}).delay(1000).show().live('click',function(){
            $(this).fadeOut("slow");
        });
    }
    
    // scroll to
    var $scrollTos = $('a.scroll-to');
    if($scrollTos.length){
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            $scrollTos.live('click',function(e){
                var $this = $(this);
                $.scrollTo($($this.attr('href')),1000,{axis:'y'});
            });
        });
    }

    // checando features
    if(!head.placeholder){
        var $elm;

        $('[placeholder]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue(); 
        });
    }
    
    if(!head.number){
        var $elm;

        $('input[type=number]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue').addClass('mask-int')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue();

            _onlyNumbers('.mask-int');
        });
    }

    // old ie
    if($.browser.msie && $.browser.version < 9){
        // old ie hacks
    }

    $('.input-number').live('blur',function(){
        var $this = $(this),
            val = Number($this.val());

        if($this.attr('max')) {
            var max = Number($this.attr('max'));
            
            if(max < val) {
                alert('O valor está acima do permitido ('+max+')');
                window.setTimeout(function(){$this.focus();},100);
            }
        }

        if($this.attr('min')) {
            var min = Number($this.attr('min'));

            if(min > val) {
                alert('O valor está abaixo do permitido ('+min+')');
                window.setTimeout(function(){$this.focus();},100);
            }
        }
    });

    checkMobile();
    $(window).resize(function(e){
        checkMobile();
    });
});

// Color Hex <-> RGB
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h){return (h.charAt(0)=="#") ? h.substring(1,7):h}
function hexToRGB(h){return {'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)}}
function RGBToHex(r,g,b){var dec = r+256*g+65536*b;return dec.toString(16);}

// Math
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}

// ValidateForm
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert2($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};

// Currency
function currency(val){return 'R$ '+(val+'').replace('.',',');}

// valida campo vazio
function isEmpty(elm) {
    var v = $.trim(elm.val());

    return v=='' || v==elm.data('prevalue') || v=='__none__';
}

// helper console.log
function _log(log) {
    if(typeof(window.console)=='undefined'){
        return alert2(log);
    }

    return console.log(log);
}
function _d(log){ _log(log); } // alias para _log()

// 9 digitos
function digito9(elm,options){
    var ddd9digit = ['11','12','13','14','15','16','17','18','19'], // ddd's que precisam de 9 dig
        tp9digit  = ['cel'], // tipos que precisam de 9 dig
        dddEmpty = ['','__','ddd: *','ddd','ddd:'],
        $this = $(elm),
        opts = {
            'premask' : false,
            'ddd'     : 'input.mask-ddd',
            'tipotel' : 'select.tipotel'
        },
        opts2 = options || {};
        

    var opt = $.extend(opts,opts2);
    
    var dddField      = $(opt.ddd),
        ddd           = dddField.val(),
        tipotelField  = opt.tipotel ? $(opt.tipotel) : null,
        tipotel       = tipotelField ? tipotelField.val() : 'cel';
    
    // função que checa se precisa adicionar o dig
    var has9digit = function(){
        if($.inArray(dddField.val(),ddd9digit)!=-1){
            if(tipotelField){
                if($.inArray(tipotelField.val(),tp9digit)!=-1){
                    return true;
                }
            } else {
                if($.inArray(tipotel,tp9digit)!=-1){
                    return true;
                }
            }
        }
            
        return false;
    };

    if(opt.premask) $this.mask(has9digit()?'99999-9999':'9999-9999');

    $this.live('focus',function(){
            // console.log([$.trim(dddField.val().toLowerCase()),opt.tipotel]);
            $this.unmask();

            if($.inArray($.trim(dddField.val().toLowerCase()),dddEmpty)!=-1) {
                    $this.blur();
                    dddField.focus();
                    alert2('Digite seu DDD');
                    return false;
            }

            $this.mask(has9digit()?'99999-9999':'9999-9999');
    });
}

function _resetForm(formSelector){
    $(formSelector).find('input,textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}

/* onlyNumbers */
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

// head.js features
head.feature('placeholder', function() {
    var t = document.createElement('textarea'); // #=> <textarea></textarea>
    return (t.placeholder !== undefined); // #=> true || false
});

head.feature('number', function() {
    var t = document.createElement('input'); 
    // if(!$.browser.msie) t.type = 'number'; // #=> <input type="number">
    return (t.max !== undefined); // #=> true || false
});

// loaders
function _loadFancybox(callback){
    if(fbLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
        head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
            fbLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadCycle(callback){
    if(cycleLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/cycle.min.js',function(){
            cycleLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _sto(f,t) { window.setTimeout(f,(t||1)*1000); }

function validaNome(nome) {
    // valida pelo menos 1 nome e 1 sobrenome
    // com pelo menos 2 letras cada
    // var reTipo = /[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛç]{2,}[ ]{1,}[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛç]{2,}/;
    var reTipo = /[a-zA-ZáàâãÁÀÂÃéèêÉÈÊíìÍÌóòôõÓÒÔÕúùûÚÙÛç]{2,}/;
    if(!reTipo.test(nome)) return false;

    // verifica se possui numeros
    // var reNum = /[0-9]/;
    // if(reNum.test(nome)) return false;

    return true;
}

function validaEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkMobile(){
    windowWidth = $(window).width(); // window.innerWidth
    isDesktop = windowWidth >= 1200;
    isMobile = windowWidth < 1200;
    isTablet = windowWidth < 1200 && windowWidth >= 768;
    isSmartphone = windowWidth < 767;

    var $html = $('html');

    var log = windowWidth;

    if(isDesktop){
        
    }
    if(isMobile){ }
    if(isTablet){ }
    if(isSmartphone){ }

    // if(APP_ENV=='development') console.log(log);
}