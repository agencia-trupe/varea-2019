// console.log('publicacoes');

$('.frm-publicacao').submit(function(e){
	e.preventDefault();

	var $form = $(this),
		$nome = $('input[name=nome]',this),
		$email = $('input[name=email]',this),
		$id = $('input[name=id]',this),
		$status = $('.status',this).removeClass('error'),
		$confirm = $form.next('.publicacao-dl-confirm'),
		url = this.action+'.json',
		data = $form.serialize();

	if(!validaNome($nome.val())){
		$status.addClass('error').text('Nome inválido *');
		$nome.focus();
		return false;
	}
	if(!validaEmail($email.val())){
		$status.addClass('error').text('E-mail inválido *');
		$email.focus();
		return false;
	}
	$status.text('Aguarde...');

	$.post(url,data,function(json){
		// console.log(json); return;
		if(json.error) {
			$status.addClass('error').text(json.error);
			return false;
		}

		$nome.val('');
		$email.val('');
		$status.text('');
		$form.slideUp('fast',function() {
			$confirm.html(json.msg).slideDown('fast');
		});

		// if(json.file) location.href = json.file;
	},'json');

	return false;
});


function _test() {
	window.setTimeout(function(){
		$('a.publicacao:first-child').trigger('click');
		
		$('#publicacao-dl-form-1 input[name=nome]').val('teste');
		$('#publicacao-dl-form-1 input[name=email]').val('teste'+SITE_NAME+'@mailinator.com');
		window.setTimeout(function(){
			// $('#publicacao-dl-form-1 form').submit();
		},500);
	},500);
}
if(ENV_DEV) _test();

