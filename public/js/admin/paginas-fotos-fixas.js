/*global $ */
$(document).ready(function () {
    $.getCss(JS_PATH+'/fileinput/fileinput.css');
    head.js(JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
        $('.fileinput').each(function(){
            var $this = $(this),
                iText = $this.data('arquivo') || 'Selecionar imagem',
                bText = $this.data('button');
            $this.fileinput({
                inputText: iText,
                buttonText: bText
            });
        });
        //$('#imagem_id2.hide').parent('div').hide();

        $('#paginas_fotos_fixas .ui-corner-right').removeClass('ui-corner-right');
        $('#paginas_fotos_fixas .ui-corner-left').removeClass('ui-corner-left');
        $('#paginas_fotos_fixas .ui-widget-content').css('background','#F8F8F8');
    });
});
