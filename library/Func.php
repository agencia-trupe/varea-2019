<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }
}

function _d($var,$exit=true){ return Is_Var::dump($var,$exit); }
function _e($var,$exit=true){ return Is_Var::export($var,$exit); }

function export_url($section,$ext)
{
    $url = explode('/'.$section,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return implode('/',array($url[0],$section,'export.'.$ext.$url[1]));
}
function export_url_report($section,$action,$ext)
{
    $url = explode('/'.$section.'/'.$action,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    $qs  = (bool)trim($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'].'&' : '?';
    return implode('/',array($url[0],$section,$action)).$qs.'export='.$ext;
}

function _currency($v,$view=null)
{
    return $view ? $view->currency($v) : 'R$ '.number_format($v,2,',','.');
}

function _currencyParcel($v,$parc,$view=null)
{
    return $view ? $view->currency($v/$parc) : 'R$ '.number_format($v/$parc,2,',','.');
}

function _utfRow($row){ return Is_Array::utf8DbRow($row); }

function _utfRows($rows){ return Is_Array::utf8DbResult($rows); }

function dtbr($dt,$sep='/',$withTime=false,$time_sep="h",$withSec=false){
    return $withTime ? 
        Is_Date::am2brWithTime($dt,$sep,$time_sep,$withSec):
        Is_Date::am2br($dt,$sep);
}
function dtam($dt,$sep='-',$withTime=false,$time_sep="h",$withSec=false){
    return $withTime ? 
        Is_Date::br2amWithTime($dt,$sep,$time_sep,$withSec):
        Is_Date::br2am($dt,$sep);
}

function cleanHtml($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
        }
    }
    
    return $html->html();
    // return $text;
}

function _parseEndereco($dados)
{
    $endereco = $dados->endereco;
    $bairro = $dados->bairro;
    $cidade = $dados->cidade;
    $estado = $dados->estado;

    $endereco = reset(explode(' cj.', $endereco));
    $endereco = reset(explode(' cj ', $endereco));
    $endereco = reset(explode(' conj.', $endereco));
    $endereco = reset(explode(' conj ', $endereco));
    $endereco = reset(explode(' Cj.', $endereco));
    $endereco = reset(explode(' Cj ', $endereco));
    $endereco = reset(explode(' Conj.', $endereco));
    $endereco = reset(explode(' Conj ', $endereco));
    $endereco = reset(explode(' - ', $endereco));
    $bairro = reset(explode(' - ', $bairro));
    
    return implode(', ',array($endereco,$bairro,$cidade,$estado));
}

function pedidoDataHora($data)
{
    if(!(bool)trim(@$data)) return array('','','');
    
    $pedido_datahora = $data;
    $p_datahora = explode(' ',$pedido_datahora);
    $p_data = Is_Date::am2br($p_datahora[0]);
    $p_hora = substr($p_datahora[1],0,-3);
    $p_datahora = $p_data.' '.$p_hora;

    return array($p_data,$p_hora,$p_datahora);
}

function pedidoFormaEntrega($taxas)
{
    $forma = '';

    foreach($taxas as $taxa)
        if(strstr($taxa->descricao,'orreio'))
            $forma = trim(end(explode('-',$taxa->descricao)));

    return $forma;
}

/**
 * Retorna URL da página para ser usado em templates, email, etc.
 * (não é usada a define URL para ser a mesma em ambos os ambientes)
 */
function site_url($withHttp=true)
{
    return ($withHttp ? (IS_SSL ? 'https://' : 'http://') : '').'institutoph.com.br';
}

function site_link($anchor=null, $attrs='')
{
    return '<a href="'.site_url().'" '.$attrs.'>'.($anchor ? $anchor : site_url(false)).'</a>';
}

/**
 * Auxliares para emails
 */
function mail_rodape()
{
    return '<br>'.
           // '<p>Em caso de dúvidas, por favor entre em contato através de nosso email <a href="mailto:contato@'.SITE_NAME.'.com.br">contato@'.SITE_NAME.'.com.br</a>.<br><br>'.
           // 'Horário de atendimento, de segunda à sexta das 10:00 as 16:00.</p><br>'.
           '<p>Atenciosamente,<br>'.SITE_TITLE.'<br>'.site_link().'</p>';
}
