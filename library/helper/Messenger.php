<?php

class Helper_Messenger extends Zend_Controller_Action_Helper_FlashMessenger
{
    public function addMessage($message,$type='',$expirationHops=1)
    {
        if (self::$_messageAdded === false) {
            self::$_session->setExpirationHops($expirationHops, null, true);
        }

        if (!is_array(self::$_session->{$this->_namespace})) {
            self::$_session->{$this->_namespace} = array();
        }
		
		$helper_message = new stdClass();
		$helper_message->message = $message;
		$helper_message->type    = $type;
		
        self::$_session->{$this->_namespace}[] = $helper_message;

        return $this;
    }
	
	public function getCurrentMessages(){
		$cm = parent::getCurrentMessages();
		if((bool)$cm) self::clearCurrentMessages();
		return $cm;
	}
}
