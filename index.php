<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));
defined('APP_PATH') || define('APP_PATH', APPLICATION_PATH);

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
defined('APP_ENV') || define('APP_ENV', APPLICATION_ENV);
defined('ENV_DEV') || define('ENV_DEV', APP_ENV=='development');
defined('ENV_PRO') || define('ENV_PRO', APP_ENV=='production');
defined('ENV_TEST')|| define('ENV_TEST',APP_ENV=='testing'||APP_ENV=='staging');
defined('DS') || define('DS', DIRECTORY_SEPARATOR);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/models'),
    get_include_path(),
)));

function __autoload($class){
    require_once(implode("/",explode("_",$class)).".php");
}

//Is_Var::dump(get_include_path());exit();

/** ******************************************
 * Libs
 ****************************************** */
Lib::import('*');
Lib::import('helper.*');
/** ******************************************
 * /Libs
 ****************************************** */

/** ******************************************
 * Rotas personalizadas
 ****************************************** */
$routes = array(
    'img' => new Zend_Controller_Router_Route('img/:img/*',array(
        'controller' => 'img',
        'action'     => 'index'
    )),
    'min' => new Zend_Controller_Router_Route('min/:type/:files',array(
        'controller' => 'min',
        'action'     => 'index'
    )),
    'siteonepage' => new Zend_Controller_Router_Route_Regex('(areas-atuacao|advogados|contato)',array(
        'controller' => 'index',
        'action'     => 'index'
    // ),array(1 => 'action')),
    )),
    'busca' => new Zend_Controller_Router_Route('busca/:busca/',array(
        'controller' => 'busca',
        'action'     => 'index'
    )),
    'professores' => new Zend_Controller_Router_Route('professores/:alias/',array(
        'controller' => 'professores',
        'action'     => 'interna'
    )),
    'cursos' => new Zend_Controller_Router_Route('cursos/:alias/',array(
        'controller' => 'cursos',
        'action'     => 'interna'
    )),
    'grupos' => new Zend_Controller_Router_Route('grupos/:alias/',array(
        'controller' => 'grupos',
        'action'     => 'interna'
    )),
    'publicacoes' => new Zend_Controller_Router_Route('publicacoes/:alias/',array(
        'controller' => 'publicacoes',
        'action'     => 'interna'
    )),
    'publicacoes-enviar' => new Zend_Controller_Router_Route('publicacoes/enviar',array(
        'controller' => 'publicacoes',
        'action'     => 'enviar'
    )),
    'publicacoes-enviar-json' => new Zend_Controller_Router_Route('publicacoes/enviar.json',array(
        'controller' => 'publicacoes',
        'action'     => 'enviar'
    )),
    'admin-edit' => new Zend_Controller_Router_Route('admin/:controller/edit/:id/',array(
        'module'     => 'admin',
        'controller' => ':controller',
        'action'     => 'edit'
    )),
    'paginas-edit' => new Zend_Controller_Router_Route('admin/paginas/edit/:alias/',array(
        'module'     => 'admin',
        'controller' => 'paginas',
        'action'     => 'edit'
    )),
);

$front = Zend_Controller_Front::getInstance();
$router = $front->getRouter();
$router->addRoutes($routes);
/** ******************************************
 * /Rotas personalizadas
 ****************************************** */

$router = new Zend_Controller_Router_Rewrite();
$request = new Zend_Controller_Request_Http();
$router->route($request);

/** ******************************************
 * Defines
 ****************************************** */
defined('SITE_NAME')
    || define('SITE_NAME', "varea");

defined('SITE_TITLE')
    || define('SITE_TITLE', "Varea Dionisio Advogados");

defined('SITE_DESCRIPTION')
    || define('SITE_DESCRIPTION', "");

defined('SITE_KEYWORDS')
    || define('SITE_KEYWORDS', "");

// defined('SITE_ANALYTICS')
    // || define('SITE_ANALYTICS', "");

defined('ROOT_PATH')
    || define('ROOT_PATH', $request->getBaseUrl());

defined('IS_DOMAIN')
    || define('IS_DOMAIN', !(bool)strstr(ROOT_PATH, SITE_NAME) && !(bool)strstr(ROOT_PATH, 'previa'));

defined('IS_SSL')
    || define('IS_SSL', (bool)$_SERVER['HTTPS']);

defined('URL')
    || define('URL', (IS_SSL ? 'https' : 'http')."://".$_SERVER['HTTP_HOST'].ROOT_PATH);

defined('FULL_URL')
    || define('FULL_URL', strtolower(URL.str_replace(ROOT_PATH.'','',$_SERVER['REQUEST_URI'])));

defined('SCRIPT_MIN')
    || define('SCRIPT_MIN', 'load'); // 'load' or 'min'

defined('SCRIPT_RETURN_PATH')
    || define('SCRIPT_RETURN_PATH', IS_DOMAIN ? '.' : '..');

defined('JS_PATH')
    || define('JS_PATH', ROOT_PATH.'/public/js');
defined('FULL_JS_PATH')
    || define('FULL_JS_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".JS_PATH);
define('JS_URL',URL.'/public/js');

defined('CSS_PATH')
    || define('CSS_PATH', ROOT_PATH.'/public/css');
defined('FULL_CSS_PATH')
    || define('FULL_CSS_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".CSS_PATH);
define('CSS_URL',URL.'/public/css');
    
defined('IMG_PATH')
    || define('IMG_PATH', ROOT_PATH.'/public/images');
defined('FULL_IMG_PATH')
    || define('FULL_IMG_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".IMG_PATH);
define('IMG_URL',URL.'/public/images');
    
defined('FILE_PATH')
    || define('FILE_PATH', ROOT_PATH.'/public/files');
defined('FULL_FILE_PATH')
    || define('FULL_FILE_PATH', APP_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH);
define('FILE_URL',URL.'/public/files');

defined('DEFAULT_LANGUAGE')
    || define('DEFAULT_LANGUAGE', 'pt');
    
/** ******************************************
 * /Defines
 ****************************************** */

/** Zend_Application */
//require_once 'Zend/Application.php';

// load configuration
$config = new Zend_Config_Ini('./application/configs/application.ini',APPLICATION_ENV);

// setup database
$dbAdapter = Zend_Db::factory(
    $config->resources->db->adapter,
    $config->resources->db->params->toArray()
);
Zend_Db_Table::setDefaultAdapter($dbAdapter);

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
            
